import {
  ActionType,
  CARDS_LOADING,
  CARDS_LOADING_ERROR,
  CARDS_LOADING_SUCCESS,
  GET_ALL,
} from "../types/cards.type";

const initialState = {
  items: [],
};

const cards = (state = initialState, action: ActionType) => {
  switch (action.type) {
    case GET_ALL:
      return {
        ...state,
        items: action.payload,
      };
    case CARDS_LOADING:
      return {
        ...state,
        items: ["...loading"],
      };
    case CARDS_LOADING_SUCCESS:
      return {
        ...state,
        items: action.payload,
      };
    case CARDS_LOADING_ERROR:
      return {
        ...state,
        items: action.payload,
      };
    default:
      return state;
  }
};

export default cards;
