import { configureStore } from "@reduxjs/toolkit";
import cards from "./cardsReducer";

export const store = configureStore({
  reducer: {
    cards: cards,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
// export type RootState = ReturnType<typeof rootReducer>;
