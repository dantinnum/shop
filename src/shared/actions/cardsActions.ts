import { CardModel } from "../../models/card";
import { getCards } from "../../services/httpService";
import { ActionType, GET_ALL } from "../types/cards.type";

export const fetchCardsLoading = () => ({ type: "CARDS_LOADING" });
export const fetchSuccessCardsLoading = (data: any) => ({
  type: "CARDS_LOADING_SUCCESS",
  payload: data,
});
export const fetchErrorCardsLoading = (error: any) => ({
  type: "CARDS_LOADING_ERROR",
  payload: error,
});

export const getAll = (cards: CardModel[]): ActionType => {
  return {
    type: GET_ALL,
    payload: cards,
  };
};

export const fetchCards = () => async (dispatch: any) => {
  try {
    dispatch(fetchCardsLoading());
    const data = await getCards();
    dispatch(fetchSuccessCardsLoading(data));
    return data;
  } catch (error) {
    dispatch(fetchErrorCardsLoading(error));
  }
};
