import { CardModel } from "../../models/card";

export const GET_ALL = "GET_ALL";
export const FETCH_CARDS_BEGIN = "FETCH_CARDS_BEGIN";
export const CARDS_LOADING = "CARDS_LOADING";
export const CARDS_LOADING_SUCCESS = "CARDS_LOADING_SUCCESS";
export const CARDS_LOADING_ERROR = "CARDS_LOADING_ERROR";

interface IGetCards {
  type: typeof GET_ALL;
  payload: CardModel[];
}

interface ICardsLoading {
  type: typeof CARDS_LOADING;
}

interface ICardsSuccessLoading {
  type: typeof CARDS_LOADING_SUCCESS;
  payload: CardModel[];
}

interface ICardsErrorLoading {
  type: typeof CARDS_LOADING_ERROR;
  payload: any;
}

export type ActionType =
  | IGetCards
  | ICardsLoading
  | ICardsSuccessLoading
  | ICardsErrorLoading;
