import * as S from "./Cards.style";
import basket from "../../icons/basket.svg";
import heart from "../../icons/heart.svg";
import heart_filled from "../../icons/heart_filled.svg";
import { useState } from "react";

interface CardProps {
  imgUrl: string;
  description: string;
  price: number;
}

export const Card: React.FC<CardProps> = ({ imgUrl, description, price }) => {
  const [selected, setSelected] = useState(false);

  return (
    <S.Card>
      <S.CardBody>
        <S.Img src={imgUrl} alt="кушай сидя" />
        <S.Description>{description}</S.Description>
      </S.CardBody>
      <S.CardFooter>
        <S.Price>{price} ₽</S.Price>
        <S.Basket src={basket} width="24px" alt="корзина" />
      </S.CardFooter>
      <S.Heart
        src={selected ? heart_filled : heart}
        onClick={() => setSelected(!selected)}
        alt="серце"
      />
    </S.Card>
  );
};
