import styled, { keyframes } from "styled-components";

const heartBeat = keyframes`
    to {
        transform: scale(1.1);
    }
`;

export const Cards = styled.div`
  display: flex;
  flex-direction: row;
  width: 720px;
  height: 100%;
  flex-wrap: wrap;
  justify-content: center;
`;

export const Card = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  margin: 12px;
  width: 180px;
  transition: box-shadow 0.25s;
  padding: 8px;
  border-radius: 6px;
  box-shadow: 0px 0px 4px 0px grey;
  position: relative;

  &:hover {
    box-shadow: 0px 0px 8px 0px black;
  }
`;

export const Description = styled.div`
  font-size: 12px;
  color: grey;
  margin: 8px 0;
  font-family: sans-serif;
  overflow: hidden;
  display: -webkit-box;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 5;
`;

export const Img = styled.img`
  float: left;
  width: 180px;
  height: 240px;
  object-fit: cover;
`;

export const CardFooter = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const CardBody = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Price = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
`;

export const Basket = styled.img`
  cursor: pointer;
`;

export const Heart = styled.img`
  cursor: pointer;
  position: absolute;
  top: 16px;
  right: 16px;
  height: 24px;

  &:hover {
    animation: ${heartBeat} 1s infinite;
  }
`;
