import * as S from "./Cards.style";
import { Card } from "./Card";
import { CardModel } from "../../models/card";
import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../shared/reducers/rootReducer";
import { fetchCards } from "../../shared/actions/cardsActions";

interface CardsProps {
  cards: CardModel[];
  loadCards: () => void;
}

export const Cards = () => {
  const { items } = useSelector((state: RootState) => state.cards);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchCards() as any);
  }, [dispatch]);

  return (
    <S.Cards>
      {items.map((card: CardModel) => (
        <Card
          key={card.id}
          imgUrl={card.image}
          description={card.description}
          price={card.price}
        />
      ))}
    </S.Cards>
  );
};
