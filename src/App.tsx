import React from "react";
import { Provider } from "react-redux";
import "./App.css";
import { Cards } from "./components/cards/Cards";
import { store } from "./shared/reducers/rootReducer";
// import { rootReducer } from "./shared/reducers/rootReducer";

// const store = createStore(rootReducer, applyMiddleware(thunk));

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <header className="App-header">Шапка</header>
        <main className="App-main">
          <div className="App-body">
            <Cards />
          </div>
        </main>
      </Provider>
    </div>
  );
}

export default App;
